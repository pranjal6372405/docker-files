Let's experiment with how installing something into a container at runtime behaves!

***Note:** Modifying the contents of a container at runtime is not something you would normally do. We are doing it here for instructional purposes only!*


```bash
# Create a container from the ubuntu image
docker run --interactive --tty --rm ubuntu:22.04

# Try to ping google.com
ping google.com -c 1 # This results in `bash: ping: command not found`

# Install ping
apt update
apt install iputils-ping --yes

ping google.com -c 1 # This time it succeeds!
exit
```

We generally never want to rely on a container to persist the data, so for a dependency like this, we would want to include it in the image:

```bash
# Build a container image with ubuntu image as base and ping installed
docker build --tag my-ubuntu-image -<<EOF
FROM ubuntu:22.04
RUN apt update && apt install iputils-ping --yes
EOF

# Run a container based on that image
docker run -it --rm my-ubuntu-image

# Confirm that ping was pre-installed
ping google.com -c 1 # Success! 🥳
```

The `FROM... RUN...` stuff is part of what is called a `Dockerfile` that is used to specify how to build a container image. We will go much deeper into building containers later in the course, but for now just understand that for anything we need in the container at runtime we should build it into the image! 

The one exception to this rule is environment specific configuration (environment variables, config files, etc...) which can be provided at runtime as a part of the environment (see: https://12factor.net/config).

```bash
# Create a container from the ubuntu image
docker run -it --rm ubuntu:22.04

# Make a directory and store a file in it
mkdir my-data
echo "Hello from the container!" > /my-data/hello.txt

# Confirm the file exists
cat my-data/hello.txt
exit
```

#### i. Volume Mounts
We can use volumes and mounts to safely persist the data.

```bash
# create a named volume
docker volume create my-volume

# Create a container and mount the volume into the container filesystem
docker run  -it --rm --mount source=my-volume,destination=/my-data/ ubuntu:22.04
# There is a similar (but shorter) syntax using -v which accomplishes the same
docker run  -it --rm -v my-volume:/my-data ubuntu:22.04

# Now we can create and store the file into the location we mounted the volume
echo "Hello from the container!" > /my-data/hello.txt
cat my-data/hello.txt
exit
```

#### ii. Bind Mounts

Alternatively, we can mount a directory from the host system using a bind mount:

```bash
# Create a container that mounts a directory from the host filesystem into the container
docker run  -it --rm --mount type=bind,source="${PWD}"/my-data,destination=/my-data ubuntu:22.04
# Again, there is a similar (but shorter) syntax using -v which accomplishes the same
docker run  -it --rm -v ${PWD}/my-data:/my-data ubuntu:22.04

echo "Hello from the container!" > /my-data/hello.txt

# You should also be able to see the hello.txt file on your host system
cat my-data/hello.txt
exit

#### Postgres 
https://hub.docker.com/_/postgres
```bash
docker run -d --rm \
  -v pgdata:/var/lib/postgresql/data \  # Mounting a volume for PostgreSQL data
  -e POSTGRES_PASSWORD=foobarbaz \      # Setting the PostgreSQL password
  -p 5432:5432 \                         # Exposing PostgreSQL port
  postgres:15.1-alpine                   # Using the PostgreSQL 15.1 Alpine image
```

#### Mongo
https://hub.docker.com/_/mongo
```bash
docker run -d --rm \
  -v mongodata:/data/db \                   # Mounting a volume for MongoDB data
  -v ${PWD}/mongod.conf:/etc/mongod.conf \  # Mounting custom mongod.conf file
  -e MONGO_INITDB_ROOT_USERNAME=root \      # Setting the root username for MongoDB
  -e MONGO_INITDB_ROOT_PASSWORD=foobarbaz \ # Setting the root password for MongoDB
  -p 27017:27017 \                          # Exposing MongoDB port
  mongo:6.0.4 --config /etc/mongod.conf     # Passing custom config file path to MongoDB
```

#### Redis
https://hub.docker.com/_/redis
```bash
docker run -d --rm \
  -v redisdata:/data \
  redis:7.0.8-alpine
docker run -d --rm \
  -v redisdata:/data \
  -v ${PWD}/redis.conf:/usr/local/etc/redis/redis.conf \
  redis:7.0.8-alpine redis-server /usr/local/etc/redis/redis.conf
```

#### MySQL
https://hub.docker.com/_/mysql
```bash
docker run -d --rm \
  -v mysqldata:/var/lib/mysql \
  -e MYSQL_ROOT_PASSWORD=foobarbaz \
  mysql:8.0.32
docker run -d --rm \
  -v mysqldata:/var/lib/mysql \
  -v ${PWD}/conf.d:/etc/mysql/conf.d \
  -e MYSQL_ROOT_PASSWORD=foobarbaz \
  mysql:8.0.32

```

#### Elasticsearch
https://hub.docker.com/_/elasticsearch
```bash
docker run -d --rm \
  -v elasticsearchdata:/usr/share/elasticsearch/data \
  -e ELASTIC_PASSWORD=foobarbaz \
  -e "discovery.type=single-node" \
  -p 9200:9200 \
  -p 9300:9300 \
  elasticsearch:8.6.0

```

#### Neo4j
https://hub.docker.com/_/neo4j

```bash
docker run -d --rm \
    -v=neo4jdata:/data \
    -e NEO4J_AUTH=neo4j/foobarbaz \
    -p 7474:7474 \
    -p 7687:7687 \
    neo4j:5.4.0-community
```

#!/bin/bash

# Operating systems
docker run -it --rm ubuntu:22.04
docker run -it --rm debian:bullseye-slim
docker run -it --rm alpine:3.17.1
docker run -it --rm busybox:1.36.0

# Programming runtimes
docker run -it --rm python:3.11.1
docker run -it --rm node:18.13.0
docker run -it --rm php:8.1
docker run -it --rm ruby:alpine3.17

# CLI Utilities

# jq (JSON command-line utility)
docker run -i stedolan/jq <sample-data/test.json '.key_1 + .key_2'

# yq (YAML command-line utility)
docker run -i mikefarah/yq <sample-data/test.yaml '.key_1 + .key_2'

# sed (stream editor)
docker run -i --rm busybox:1.36.0 sed 's/file./file!/g' <sample-data/test.txt

# base64

# Pipe input from previous command
echo "This string is just long enough to trigger a line break in GNU base64." | docker run -i --rm busybox:1.36.0 base64

# Read input from file
docker run -i --rm busybox:1.36.0 base64 </sample-data/test.txt

# Amazon Web Services CLI
docker run --rm -v ~/.aws:/root/.aws amazon/aws-cli:2.9.18 s3 ls

# Google Cloud Platform CLI
docker run --rm -v ~/.config/gcloud:/root/.config/gcloud gcr.io/google.com/cloudsdktool/google-cloud-cli:415.0.0 gsutil ls

# Improving the Ergonomics

# For yq

# Shell function
yq-shell-function() {
  docker run --rm -i -v ${PWD}:/workdir mikefarah/yq "$@"
}
yq-shell-function <sample-data/test.yaml '.key_1 + .key_2'

# Alias
alias 'yq-alias=docker run --rm -i -v ${PWD}:/workdir mikefarah/yq'
yq-alias <sample-data/test.yaml '.key_1 + .key_2'





